package nl.utwente.di.tempConverter;

public class Converter {

    public double celsiusToFahrenheit(double temp) {
        return (temp * 1.8) + 32;
    }
}
